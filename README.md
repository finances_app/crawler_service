# How to debug this thing:

Run the server.js script with the following environment variables:
- CRAWLER_CONFIG_PATH=../crawler_secrets.json
- PORT=9095
- NODE_ENV=PROD

With Postman, send the query:
```
POST localhost:9095/account/desjardins/<userId>/scrape {
    "userArgs": []
}
```

Setting the NODE_ENV to DEV will start the crawler with `headless: false`