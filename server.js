const express = require('express');
const bodyParser = require('body-parser');
const fs = require('fs');
const util = require('util');
const ms = require('ms');

const AccesdCrawler = require('./crawlers/AccesdHtmlCrawler');
const DemoDataCrawler = require('./crawlers/DemoDataCrawler');

const fs_readFile = util.promisify(fs.readFile);

const app = express();

app.use(bodyParser.urlencoded({ extended: true, limit: '5mb' }));
app.use(bodyParser.json({ limit: '5mb' }));

app.post('/account/demo/:user_id/scrape', async (req, res) => {
    /**
     * @property {string} user_id
     */
    const params = req.params;

    let crawler = new DemoDataCrawler({ userId: params.user_id });

    try {
        let response = await crawler.getTransactions();
        res.send(response);
    } catch (err) {
        console.error(err);
        res.status(500).send({
            message: err.message,
            stack: err.stack,
        });
    }
});

const activeCrawlerMap = new Map();
app.post('/account/desjardins/:user_id/:operation', async (req, res) => {
    /**
     * @property {string} user_id
     * @property {string} [operation]
     */
    const params = req.params;
    /**
     * @property {string} userArgs
     * @property {string} userEmail
     * @property {string} [accountId]
     * @property {number} [amount]
     */
    const body = req.body;

    const userArgs = body.userArgs;
    const daysToFetch = parseInt(req.query.days || '32');

    const secretData = await fs_readFile(process.env.CRAWLER_CONFIG_PATH);
    const secrets = JSON.parse(secretData);
    const userSecrets = secrets[params.user_id];

    if (!userSecrets) {
        return res.status(401).send({
            message: `Missing credentials for the crawler.`,
        });
    }

    const sessionId = params.user_id;
    try {
        let sessionInfo = activeCrawlerMap.get(sessionId);
        let crawler = null;

        if (sessionInfo) {
            crawler = sessionInfo.crawler;
        } else {
            crawler = new AccesdCrawler({
                credentials: {
                    cardNumber: userSecrets.desjardins.account_number,
                    password: userSecrets.desjardins.password,
                    securityPhrase: userSecrets.desjardins.secret_phrase,
                },
                userId: params.user_id,
            });
            sessionInfo = {
                crawler,
                timeoutHandle: null
            };
            activeCrawlerMap.set(sessionId, sessionInfo);
        }

        let response = {};
        switch (params.operation) {
            case 'scrape': {
                response = await crawler.scrapeContent(daysToFetch);
                activeCrawlerMap.delete(sessionId);
                break;
            }
            // series of commands to allow the CLI to anwser the secret question or configure the 2fa
            case 'manual_login': {
                // Attempt to log in and navigate to the home page.
                // The response indicates if the log in succeded or if a manuel step is required.
                // The session is automaticaly closed 5 minutes after the last action, unless the 'close_session' is called.
                const res = await crawler.manualLogin();
                sessionInfo.timeoutHandle = setTimeout(() => {
                    activeCrawlerMap.delete(sessionId);
                    crawler.endSession(res.success);
                }, ms('5min'));
                activeCrawlerMap.set(sessionId, sessionInfo);

                response = {
                    success: res.success,
                    currentStep: res.currentStep,
                    question: res.question || undefined,
                };
                break;
            }
            case 'secret_question': {
                // Input the anwser to the secret question and continue the log in process.
                // Returns the same response as the 'manual_login' action
                clearTimeout(sessionInfo.timeoutHandle);

                const res = await crawler.inputSecretAnswer({ answer: userArgs[0] });
                sessionInfo.timeoutHandle = setTimeout(() => {
                    activeCrawlerMap.delete(sessionId);
                    crawler.endSession(res.success);
                }, ms('5min'));
                activeCrawlerMap.set(sessionId, sessionInfo);

                response = {
                    success: res.success,
                    currentStep: res.currentStep
                };
                break;
            }
            case '2fa': {
                // Input the 2fa code and continue the log in process.
                // Returns the same response as the 'manual_login' action.
                clearTimeout(sessionInfo.timeoutHandle);

                const res = await crawler.input2fa({ twoFactorCode: userArgs[0] });
                sessionInfo.timeoutHandle = setTimeout(() => {
                    activeCrawlerMap.delete(sessionId);
                    crawler.endSession(res.success);
                }, ms('5min'));
                activeCrawlerMap.set(sessionId, sessionInfo);

                response = {
                    success: res.success,
                    currentStep: res.currentStep
                };
                break;
            }
            case 'close_session': {
                // Log out if necessary and close the browser session.
                clearTimeout(sessionInfo.timeoutHandle);
                activeCrawlerMap.delete(sessionId);
                await crawler.endSession();
                break;
            }
            default: {
                response = {
                    success: false,
                    warnings: [`Operation '${params.operation}' not recognized.`],
                };
            }
        }

        res.send(response);
    } catch (err) {
        activeCrawlerMap.delete(sessionId);
        console.error(new Date().toLocaleString('en-US'));
        console.error(err);
        res.status(500).send({
            message: err.message,
            stack: err.stack,
        });
    }
});

app.listen(process.env.PORT || 80);
