const WebScraperUtil = require('./utils/WebScraperUtil');
const RecoverableError = require('./entities/RecoverableError');
const CriticalError = require('./entities/CriticalError');

/**
 * Abstract representation of a crawler that scrape transactions from a bank website.
 *
 * It assumes the following :
 * - It is possible to login / logout (of course)
 * - There is a page somewhere on the site that list all accounts and a way to navigate to
 *   each of them with a single click
 * - The site supports navigation by history (the "back" button)
 * - It is possible to filter the transactions of an account by "this month"
 * - [MOST IMPORTANTLY] It has no anti-crawler feature (meaning no captcha or anything similar)
 */
class AbstractBankAccountCrawler {
    /**
     * @param {object} options
     * @param {string} options.userId
     * @param {object} [options.credentials]
     * @param {string} [options.formSubmitSelector]
     */
    constructor(options) {
        /**
         * @type {{cardNumber: string, password: string, securityPhrase: string}}
         * @protected
         */
        this._credentials = options.credentials;
        /**
         *
         * @type {WebScraperUtil}
         * @protected
         */
        this._webScraper = new WebScraperUtil(options);
    }

    /**
     * Attempt to log in and navigate to the home page.
     * The response indicates if the log in succeded or if a manuel step is required.
     *
     * @returns {Promise<{success: boolean, currentStep: string, question?: string, err?: Error}>}
     */
    async manualLogin() {
        await this._init();
        return await this._manualLogin();
    }

    async input2fa(params) {
        try {
            return await this._input2fa(params);
        } catch (err) {
            await this.endSession(false);
            throw err;
        }
    }

    async inputSecretAnswer(params) {
        try {
            return await this._inputSecretAnswer(params);
        } catch (err) {
            await this.endSession(false);
            throw err;
        }
    }

    /**
     * Scrape transactions from all bank accounts of the user
     * @returns {Promise<{ success: boolean, warnings: string[], results?: object[] }>}
     */
    async scrapeContent(daysToFetch) {
        await this._init();

        let results = {};
        const loggedIn = await this._tryLogin();
        if (!loggedIn) {
            return {
                success: false,
                warnings: [],
            };
        }

        try {
            let res = await this._scrapeAccounts();
            results.html = res.html;
            results.accounts = [];
            for (let account of res.accounts) {
                try {
                    await this._navigateToAccountPage(account);
                    let res = await this._scrapeTransactionsForAccount(account, daysToFetch);
                    results.accounts.push(res);
                    await this._navigateBackToHomePage();
                } catch (err) {
                    if (err.name !== 'TimeoutError') {
                        throw err;
                    }
                    console.warn(`Timeout error occured for account (${account.type})`, err);
                }
            }
        } catch (err) {
            await this._handleCrawlerError(err);
            return {
                success: false,
                warnings: [err.message],
            };
        }

        await this.endSession();
        return {
            success: true,
            warnings: [],
            results,
        };
    }

    async _handleCrawlerError(err) {
        if (err instanceof CriticalError) {
            await this.endSession(err.logoutRequired);
            if (err.internalError) {
                err.internalError.message = err.message + ' -> [internal] ' + err.internalError.message;
                throw err.internalError;
            }
            throw err;
        }

        // everything happening from here cannot cause big issues, so we assume the error is recoverable
        if (err instanceof RecoverableError) {
            await this.endSession(err.logoutRequired);
        } else {
            await this.endSession();
        }
    }

    async _tryLogin() {
        let isLoggedIn = false;
        try {
            await this._login();
            isLoggedIn = true;
        } catch (err) {
            if (err instanceof RecoverableError) {
                await this.endSession(isLoggedIn && err.logoutRequired);
            } else if (err instanceof CriticalError && err.internalError) {
                await this.endSession(isLoggedIn && err.logoutRequired);
                err.internalError.message = err.message + ' -> [internal] ' + err.internalError.message;
                throw err.internalError;
            } else {
                await this.endSession(isLoggedIn);
                throw err;
            }
        }
        return isLoggedIn;
    }

    /**
     * @returns {Promise<void>}
     * @private
     */
    async _init() {
        await this._webScraper.init();
    }

    /**
     * @returns {Promise<void>}
     */
    async endSession(isLoggedIn = true) {
        isLoggedIn && (await this._logout());
        await this._webScraper.close();
    }

    /**
     * Internal method used for the log in process
     * @returns {Promise<{ success: boolean, currentStep: string, err?: Error }>}
     * @protected
     * @abstract
     */
    async _manualLogin() {
        throw new Error(`Method '_manualLogin' is not implemented !`);
    }

    /**
     * Input the 2-factor authentication code and continue
     * @param {object} params
     * @returns {Promise<{ success: boolean, currentStep: string, err?: Error }>}
     * @protected
     * @abstract
     */
    async _input2fa(params) {
        throw new Error(`Method '_input2fa' is not implemented !`);
    }

    /**
     * Input the secret answer and continue
     * @param {object} params
     * @returns {Promise<{ success: boolean, currentStep: string, err?: Error }>}
     * @protected
     * @abstract
     */
    async _inputSecretAnswer(params) {
        throw new Error(`Method '_inputSecretAnswer' is not implemented !`);
    }

    /**
     * Login with the user's credentials and navigate to the home page
     * @returns {Promise}
     * @protected
     * @abstract
     */
    async _login() {
        throw new Error(`Method '_login' is not implemented !`);
    }

    /**
     * Get the list of all accounts displayed on the home page.
     * @returns {Promise<{ html: string, accounts: Account[] }>}
     * @protected
     * @abstract
     */
    async _scrapeAccounts() {
        throw new Error(`Method '_scrapeAccounts' is not implemented !`);
    }

    /**
     * Navigate from the home page to the account page.
     * @param {Account} account
     * @returns {Promise<boolean>}
     * @protected
     * @abstract
     */
    async _navigateToAccountPage(account) {
        throw new Error(`Method '_navigateToAccountPage' is not implemented !`);
    }

    /**
     * Scrape all the transactions from the account page.
     * @param {Account} account
     * @param {number} daysToFetch
     * @returns {Promise<object[]>}
     * @protected
     * @abstract
     */
    async _scrapeTransactionsForAccount(account, daysToFetch) {
        throw new Error(`Method '_scrapeTransactionsForAccount' is not implemented !`);
    }

    /**
     * Navigate to the home page.
     * @returns {Promise<void>}
     * @protected
     */
    async _navigateBackToHomePage() {
        await this._webScraper.goBack();
    }

    /**
     * Log out from the bank's website
     * @returns {Promise<void>}
     * @protected
     * @abstract
     */
    async _logout() {
        throw new Error(`Method '_logout' is not implemented !`);
    }
}
module.exports = AbstractBankAccountCrawler;
