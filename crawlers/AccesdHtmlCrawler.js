const _ = require('lodash');
const dayjs = require('dayjs');
const AccountTypes = require('./entities/accountType');
const customParseFormat = require('dayjs/plugin/customParseFormat');
dayjs.extend(customParseFormat);
require('dayjs/locale/fr');
const { parse: parseHtml } = require('node-html-parser');

const AbstractBankAccountCrawler = require('./AbstractBankAccountCrawler');
const RecoverableError = require('./entities/RecoverableError');
const CriticalError = require('./entities/CriticalError');
const loginSteps = require('./entities/loginSteps');

const URLS = {
    LOGIN: 'https://accweb.mouv.desjardins.com/identifiantunique/identification',
    ACCOUNT_PAGE_ROOT: 'https://accesd.mouv.desjardins.com/sommaire-perso/sommaire/',
};
const SELECTORS = {
    INPUT_FORM_SUBMIT: 'input[type="submit"]',
    BUTTON_FORM_SUBMIT: 'button[type="submit"]',
    CARD_NUMBER_FIELD: '#codeUtilisateur',
    PASSWORD_FIELD: '#motDePasse',
    SECURITY_QUESTION: 'label[for=valeurReponse] b',
    SECURITY_ANSWER_FIELD: '#valeurReponse',
    PAGE_TITLE: 'h1[data-titre-page-mobile]', // seulement dans le context de la page de 2fa TODO: Est-ce un problème ?
    OLD_PAGE_TITLE: 'h1#titrePage',
    LOGOUT_BTN: '#entete-consultation-bouton > dsd-button',
    // 2-factor stuff and things
    TWO_FA_MODE_INPUT__TEXT_OPTION: 'choix-facteur dsd-select-tile:nth-child(1) input',
    TWO_FA_MODE_INPUT__CONTINUE_BTN: 'choix-facteur > form dsd-button:nth-child(1) button',
    TWO_FA_CODE_INPUT: 'app-saisie-code input[name=codeSecurite]',
    TWO_FA_FREQUENCY_SELECTION_LIST: 'app-saisie-code select[name=frequence]',
    TWO_FA_CODE__CONTINUE_BTN: 'app-saisie-code > form dsd-button:nth-child(1) button',

    ACCOUNT_SUMMARY_ITEMS: '#successContenuMesComptes h3.titre-produit > a',
    BANK_ACCOUNT_TABLE_ROWS: 'div#dernieresoperations div table tbody tr',

    FIRST_MONTH_OF_TRANSACTIONS: `#aucuneOperationAvecFiltresOuRecherche > div > some-element > app-transactions > div:nth-child(3) container-element:nth-child(1) > some-element > div > dsd-table table > tbody > tr`,
    SECOND_MONTH_OF_TRANSACTIONS: `#aucuneOperationAvecFiltresOuRecherche > div > some-element > app-transactions > div:nth-child(3) container-element:nth-child(2) > some-element > div > dsd-table table > tbody > tr`,
    TRANSACTION_DETAILS_DESCRIPTION:
        '#detail-transaction > dsd-container dsd-grid-row:nth-child(2) > dsd-grid-col:nth-child(2) > app-nom-au-relevee > div > p',
};
const TWO_FA_TITLE = 'Validation en 2 étapes';
const SECRET_QUESTION_TITLE = `Valider l'identité`;

/**
 * @inheritDoc
 */
class AccesdCrawler extends AbstractBankAccountCrawler {
    /**
     * @param {object} options
     * @param {object} options.credentials
     * @param {string} options.credentials.cardNumber
     * @param {string} options.credentials.password
     * @param {string} options.credentials.securityPhrase
     * @param {string} options.userId
     * @param {string} options.userEmail
     */
    constructor(options) {
        let extendedOptions = _.extend(options, { formSubmitSelector: SELECTORS.BUTTON_FORM_SUBMIT });
        super(extendedOptions);
    }

    async _checkLoginStep() {
        let scraper = this._webScraper;

        const isHomePage = await scraper.evaluate((_selectors) => {
            const summaryItems = document.querySelectorAll(_selectors.ACCOUNT_SUMMARY_ITEMS);
            return summaryItems && summaryItems.length > 0;
        }, SELECTORS);
        const loginStepTitle = await scraper.evaluate(({ SELECTORS }) => {
            let titleEl = document.querySelector(SELECTORS.PAGE_TITLE);
            let oldTitleEl = document.querySelector(SELECTORS.OLD_PAGE_TITLE);
            return (titleEl && titleEl.textContent && titleEl.textContent.trim()) ||
                (oldTitleEl && oldTitleEl.textContent && oldTitleEl.textContent.trim());
        }, { SELECTORS });

        return {
            success: !!isHomePage,
            currentStep: loginStepTitle === TWO_FA_TITLE ? loginSteps.twoFa
                : loginStepTitle === SECRET_QUESTION_TITLE ? loginSteps.secretQuestion
                : isHomePage ? loginSteps.home
                : loginSteps.other
        };
    }

    async _manualLogin({ failAt2fa } = {}) {
        let scraper = this._webScraper;
        await scraper.goto(URLS.LOGIN);
        // CARD_NUMBER_FIELD won't be available if the site is being maintained. We don't want the crawler to crash tho.
        try {
            await scraper.getPage().waitForSelector(SELECTORS.CARD_NUMBER_FIELD, { timeout: 5000 });
        } catch (err) {
            return {
                success: false,
                currentStep: loginSteps.login,
                err
            };
        }

        // try to close the pop-up
        await scraper.evaluate(() => {
            try {
                document
                    .querySelector(
                        'body > app-root > div.site.container-fluid > div.site-contenu > app-lightbox-onboarding > div > dsd-lightbox'
                    )
                    .shadowRoot.querySelector(
                    'dsd-backdrop > div > div.new.light.dsd-container.header-no-border > div.action > dsd-button > div > button'
                )
                    .click();
            } catch (err) {}
        });

        await scraper.typeIntoForm(SELECTORS.CARD_NUMBER_FIELD, this._credentials.cardNumber);
        await scraper.typeIntoForm(SELECTORS.PASSWORD_FIELD, this._credentials.password);

        await scraper.getPage().hover(SELECTORS.BUTTON_FORM_SUBMIT);
        await scraper.getPage().click(SELECTORS.BUTTON_FORM_SUBMIT);

        await scraper.sleep(15000);

        const loginStatus = await this._checkLoginStep();

        if (loginStatus.currentStep === loginSteps.twoFa) {
            if (failAt2fa) {
                await scraper.sleep(10000);
                return {
                    success: false,
                    currentStep: loginSteps.twoFa
                };
            }

            await scraper.getPage().hover(SELECTORS.TWO_FA_MODE_INPUT__TEXT_OPTION);
            await scraper.getPage().click(SELECTORS.TWO_FA_MODE_INPUT__TEXT_OPTION);

            await scraper.sleep(500);

            await scraper.getPage().hover(SELECTORS.TWO_FA_MODE_INPUT__CONTINUE_BTN);
            await scraper.getPage().click(SELECTORS.TWO_FA_MODE_INPUT__CONTINUE_BTN);

            await scraper.sleep(5000);
        } else if (loginStatus.currentStep === loginSteps.secretQuestion) {
            if (failAt2fa) {
                await scraper.sleep(10000);
                return {
                    success: false,
                    currentStep: loginSteps.secretQuestion,
                };
            }

            let secretQuestion = await scraper.evaluate((selector) => {
                let questionElement = document.querySelector(selector);
                if (questionElement) {
                    return questionElement.textContent.trim();
                } else {
                    return null;
                }
            }, SELECTORS.SECURITY_QUESTION);
            return {
                success: false,
                currentStep: loginSteps.secretQuestion,
                question: secretQuestion,
            };
        }

        await scraper.sleep(10000);
        return this._checkLoginStep();
    }

    async _input2fa({ twoFactorCode }) {
        let scraper = this._webScraper;

        const is2faPage = await scraper.evaluate(({ SELECTORS, TWO_FA_TITLE }) => {
            let titleEl = document.querySelector(SELECTORS.PAGE_TITLE);
            let twoFaCodeInput = document.querySelector(SELECTORS.TWO_FA_CODE_INPUT);
            return (titleEl && titleEl.textContent && titleEl.textContent.trim() === TWO_FA_TITLE) && !!twoFaCodeInput;
        }, { SELECTORS, TWO_FA_TITLE });
        if (!is2faPage) {
            throw new CriticalError('Tried to input 2-factor code, but the session is on the wrong page.', null, false);
        }

        await scraper.typeIntoForm(SELECTORS.TWO_FA_CODE_INPUT, twoFactorCode);
        await scraper.getPage().select(SELECTORS.TWO_FA_FREQUENCY_SELECTION_LIST, 'ne-plus-demander');

        await scraper.getPage().hover(SELECTORS.TWO_FA_CODE__CONTINUE_BTN);
        await scraper.getPage().click(SELECTORS.TWO_FA_CODE__CONTINUE_BTN);

        await scraper.sleep(15000);
        return this._checkLoginStep();
    }

    async _inputSecretAnswer({ answer }) {
        let scraper = this._webScraper;

        await scraper.typeIntoFormAndSubmit(
            SELECTORS.SECURITY_ANSWER_FIELD,
            answer
        );

        await scraper.sleep(15000);
        return this._checkLoginStep();
    }

    /**
     * @inheritDoc
     */
    async _login() {
        const res = await this._manualLogin({ failAt2fa: true });
        if (res.currentStep === loginSteps.login) {
            throw new RecoverableError(
                `Impossible d'accéder à la page de connexion. Le site est probablement en maintenance.`,
                res.err,
                false
            );
        } else if (res.currentStep === loginSteps.twoFa) {
            throw new CriticalError('2fa input required, you must log in manually to continue.', null, false);
        }
    }

    /**
     * @inheritDoc
     */
    async _scrapeAccounts() {
        await this._webScraper.getPage().waitForSelector(SELECTORS.ACCOUNT_SUMMARY_ITEMS);
        let accountUrls = await this._webScraper.evaluate((_selectors) => {
            let accountUrls = document.querySelectorAll(_selectors.ACCOUNT_SUMMARY_ITEMS);
            return [...accountUrls].map((urlEl) => urlEl.getAttribute('href'));
        }, SELECTORS);
        return {
            html: this._getPageHtml(await this._webScraper.getPage().content()),
            accounts: _.map(accountUrls, (accountUrl) => {
                let accountType = accountUrl.split('/').splice(-3).shift();
                return {
                    url: accountUrl,
                    type: accountType,
                };
            }),
        };
    }

    /**
     * @inheritDoc
     */
    async _navigateToAccountPage(account) {
        if (account.url.startsWith('https://')) {
            await this._webScraper.goto(account.url);
        } else if (account.url.includes('/sommaire-perso/sommaire-aiguilleur')) {
            // some accounts have the complete url
            await this._webScraper.goto('https://accesd.mouv.desjardins.com' + account.url);
        } else {
            // some account have a relative url
            await this._webScraper.goto(URLS.ACCOUNT_PAGE_ROOT + account.url);
        }
    }

    /**
     * @inheritDoc
     */
    async _scrapeTransactionsForAccount(account, daysToFetch) {
        let page = this._webScraper.getPage();

        // We get here before the page url has time to change sometimes. Wait a bit for the redirection to occur.
        await this._webScraper.sleep(10000);

        // figure out if this page has the new style
        const hasNewStyle = page.url().includes('sommaire-spa');
        if (!hasNewStyle) {
            // Old style account pages dont have detail pages for transaction. Simply return the html.
            await this._loadTransactions(account);
            return {
                ...account,
                html: this._getPageHtml(await page.content()),
                transactions: [],
                pendingRecords: [],
            };
        }

        await this._webScraper.waitForAnyOf([
            '#onglet-operation app-transactions container-element > some-element > dsd-table tbody > tr',
        ]);

        const transactions = [];
        transactions.push(
            ...(await this._scrapeTransactionSection(SELECTORS.FIRST_MONTH_OF_TRANSACTIONS, daysToFetch)),
            ...(await this._scrapeTransactionSection(SELECTORS.SECOND_MONTH_OF_TRANSACTIONS, daysToFetch))
        );

        const pendingRecords = [];
        switch (account.type) {
            case AccountTypes.CC: {
                pendingRecords.push(
                    ...(await this._scrapeTransactionSection(
                        `#onglet-operation app-transactions > div:nth-child(2) > container-element:nth-child(1) > some-element dsd-table tbody > tr`
                    ))
                );
                break;
            }
            // FIXME: un-comment when the credit margin list view uses the SPA
            // case AccountTypes.MC: {
            //     let creditLimit = await page.evaluate(this._extractInfoWithKey, 'Montant autorisé');
            //     accountInfo.creditLimit = importHelper.parseDesjardinsAmount(creditLimit || '0,00 $');
            //     accountInfo.balance = account.balance;
            //     break;
            // }
        }

        await this._webScraper.sleep(5000);
        return {
            ...account,
            html: this._getPageHtml(await page.content()),
            transactions,
            pendingRecords,
        };
    }

    /**
     * Filter the transactions by current statement and check if transactions are visible.
     * @param {Account} account
     * @returns {Promise<boolean>}
     * @private
     */
    async _loadTransactions(account) {
        let page = this._webScraper.getPage();
        await page.waitForSelector(SELECTORS.BANK_ACCOUNT_TABLE_ROWS);
    }

    async _scrapeTransactionSection(selector, daysToFetch) {
        const transactionElIds = await this._webScraper.evaluate((_selector) => {
            const transactionsEls = document.querySelectorAll(_selector);
            return [...transactionsEls].map((_el) => _el.id);
        }, selector);
        const page = this._webScraper.getPage();

        const transactions = [];
        for (let transactionElId of transactionElIds) {
            const hasDetails = await this._webScraper.evaluate((_elId) => {
                const transactionEl = document.getElementById(_elId);
                return !!transactionEl && transactionEl.classList.contains('cliquable');
            }, transactionElId);

            const date = await this._tryParseTransactionDate(transactionElId);
            if (date && dayjs().diff(date, 'day') > daysToFetch) {
                break;
            }

            let pageHtml = null;
            if (hasDetails) {
                await this._webScraper.evaluate((_elId) => {
                    const selectedEl = document.getElementById(_elId);
                    selectedEl && selectedEl.click();
                }, transactionElId);
                await this._webScraper.waitForAllOf([SELECTORS.TRANSACTION_DETAILS_DESCRIPTION]);
                pageHtml = this._getPageHtml(await page.content());
                await this._webScraper.evaluate(() => {
                    document
                        .querySelector('#detail-transaction > app-page-precedente > dsd-button > div > button')
                        .click();
                });
                await this._webScraper.sleep(2000);
            }
            transactions.push({
                id: transactionElId,
                html: pageHtml,
            });
        }
        return transactions;
    }

    async _tryParseTransactionDate(transactionElId) {
        try {
            const date = await this._webScraper.evaluate((elementId) => {
                const transactionEl = document.getElementById(elementId);
                const columnEls = transactionEl.querySelectorAll('td');

                const sectionTitleEl = transactionEl.parentNode.parentNode.querySelector('caption');
                let monthYearPart = sectionTitleEl.textContent.replace(/\s/gi, ' ').split(` `).slice(2).join(` `);

                return columnEls[0].textContent.trim().split(' ').shift() + ` ${monthYearPart}`;
            }, transactionElId);
            return dayjs(date.toLowerCase(), 'D MMMM YYYY', 'fr');
        } catch (err) {
            // do nothing
            return null;
        }
    }

    _getPageHtml(rawHtml) {
        const root = parseHtml(rawHtml);
        root.querySelectorAll('style').forEach((node) => node.remove());
        root.querySelectorAll('script').forEach((node) => node.remove());
        return root.toString();
    }

    /**
     * @inheritDoc
     */
    async _logout() {
        await this._webScraper.sleep(5000);

        const hasLogoutButton = await this._webScraper.evaluate((_selectors) => {
            return !!document.querySelector(_selectors.LOGOUT_BTN);
        }, SELECTORS);
        if (!hasLogoutButton) {
            await this._webScraper.goBack();
            await this._webScraper.sleep(5000);
        }

        await this._webScraper.getPage().click(SELECTORS.LOGOUT_BTN);
    }
}

module.exports = AccesdCrawler;
