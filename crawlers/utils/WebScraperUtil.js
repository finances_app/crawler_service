const fs = require('fs');
const util = require('util');
const path = require('path');
const _ = require('lodash');
const ms = require('ms');
const puppeteer = require('puppeteer-extra');
const stealthPlugin = require('puppeteer-extra-plugin-stealth');
const AdblockerPlugin = require('puppeteer-extra-plugin-adblocker');

puppeteer.use(stealthPlugin());
puppeteer.use(AdblockerPlugin({ blockTrackers: true }));

const fs_mkdir = util.promisify(fs.mkdir);

const USER_DATA_ROOT_DIR = 'puppeteer_user_data';

class WebScraperUtil {
    /**
     * @returns {object}
     * @private
     */
    get _pageLoadConfig() {
        return {
            waitUntil: ['load', 'domcontentloaded'],
        };
    }

    /**
     * @param {object} options
     * @param {string} options.userId
     * @param {string} options.formSubmitSelector
     */
    constructor(options) {
        this._userId = options.userId;
        this._formSubmitSelector = options.formSubmitSelector;
        this._page = null;
    }

    /**
     * Initialize the browser
     * @returns {Promise<void>}
     */
    async init() {
        // --- SUPER IMPORTANT TO PROVIDE AN ABSOLUTE PATH, OR CHROMIUM WILL NOT START !! ---
        let userDataDir = path.join(__dirname, '..', '..', USER_DATA_ROOT_DIR, this._userId);
        try {
            await fs_mkdir(userDataDir);
        } catch (err) {
            if (err.code !== 'EEXIST') {
                throw err;
            }
        }

        this._browser = await puppeteer.launch({
            userDataDir: userDataDir,
            args: ['--no-sandbox'], // critical option to allow 'userDataDir' to work properly
            slowMo: 100,
            // defaultViewport: { width: 411, height: 823, isMobile: true },
            defaultViewport: { width: 1920, height: 1080 },
            headless: process.env.NODE_ENV === 'PROD' || false,
        });
        this._page = await this._browser.newPage();
        this._page.setDefaultTimeout(ms('30s'));
    }

    /**
     * Getter got the page object
     * @returns {Page}
     */
    getPage() {
        return this._page;
    }

    /**
     * Shortcut for the close function of the browser
     * @returns {Promise<void>}
     */
    async close() {
        try {
            await this._browser.close();
        } catch (err) {
            console.warn(err);
        }
    }

    /**
     * Shortcut for the goto function of the page
     * @param {string} url
     * @returns {Promise<void>}
     */
    async goto(url) {
        await this._page.goto(url, this._pageLoadConfig);
    }

    /**
     * Shortcut for the goBack function of the page
     * @returns {Promise<void>}
     */
    async goBack() {
        await this._page.goBack();
    }

    /**
     * Shortcut for the evaluate function of the page
     * @returns {Promise<*>}
     */
    async evaluate() {
        return await this._page.evaluate(...arguments);
    }

    async typeIntoForm(selector, value) {
        await this._page.waitForSelector(selector);
        await this._page.focus(selector);
        await this._page.type(selector, value);
    }

    /**
     * Fill a form field with a value and submit
     * @param {string} selector
     * @param {string} value
     * @param {string} [submitSelector]
     * @returns {Promise<void>}
     */
    async typeIntoFormAndSubmit(selector, value, submitSelector = this._formSubmitSelector) {
        await this.typeIntoForm(selector, value);
        await Promise.all([this.waitForNavigation(), this._page.click(submitSelector)]);
    }

    async waitForNavigation() {
        this._page.waitForNavigation(this._pageLoadConfig);
    }

    /**
     * Extract the cell text content of a table element
     * @returns {string[][]}
     */
    async getTableData(tableRowSelector) {
        let tableEl = await this._page.$(tableRowSelector);
        if (tableEl === null) {
            return [];
        }
        return await this._page.evaluate((tableRowSelector) => {
            let results = [];
            let tableRows = document.querySelectorAll(tableRowSelector);
            for (let i = 0; i < tableRows.length; i++) {
                let cells = tableRows[i].childNodes;
                let rowData = [];
                for (let j = 0; j < cells.length; j++) {
                    let cellNode = cells[j];
                    if (cellNode.nodeType === 1)
                        // get element node only (nodeType.1), excluding empty text nodes (nodeType.3)
                        rowData.push(cellNode.textContent);
                }
                results.push(rowData);
            }
            return results;
        }, tableRowSelector);
    }

    /**
     * Wait for any one of the selectors to render in the page.
     * @param {string[]} selectors
     * @returns {Promise<boolean>}
     */
    async waitForAnyOf(selectors) {
        await Promise.race(_.map(selectors, (selector) => this._page.waitForSelector(selector)));
    }

    /**
     * Wait for any one of the selectors to render in the page.
     * @param {string[]} selectors
     * @returns {Promise<boolean>}
     */
    async waitForAllOf(selectors) {
        await Promise.all(_.map(selectors, (selector) => this._page.waitForSelector(selector)));
    }

    /**
     * Intercept a JSON XHR response matching a url part
     * @param urlPart
     * @returns {Promise<object>}
     */
    async interceptResponse(urlPart) {
        return new Promise((resolve, reject) => {
            let data = null;
            const responseHandler = async (response) => {
                try {
                    if (
                        response.request().resourceType() === 'xhr' &&
                        response.url().includes(urlPart) &&
                        data === null
                    ) {
                        data = await response.json();

                        this._page.off('response', responseHandler);
                        resolve(data);
                    }
                } catch (err) {
                    reject(err);
                }
            };
            this._page.on('response', responseHandler);
        });
    }

    async sleep(time) {
        return new Promise((resolve) => setTimeout(resolve, time));
    }
}
module.exports = WebScraperUtil;
