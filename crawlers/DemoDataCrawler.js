const _ = require('lodash');
const dayjs = require('dayjs');
dayjs.extend(require('dayjs/plugin/customParseFormat'));

const AbstractBankAccountCrawler = require('./AbstractBankAccountCrawler');
const AccountItem = require('./entities/Account');

class DemoDataCrawler extends AbstractBankAccountCrawler {
    async _login() {
        return null;
    }

    async _navigateToAccountPage(account) {
        return true;
    }

    async _scrapeAccounts() {
        return new Promise((resolve) =>
            resolve([
                new AccountItem({
                    name: 'EOP-00000',
                    type: 'EOP',
                    uniqueName: 'EOP-demo_data',
                }),
            ])
        );
    }

    async _scrapeTransactionsForAccount(account) {
        let transactions = this._buildStatementUpToNow();
        let accountInfo = {
            data: transactions,
            balance: 5000,
        };
        return _.extend(_.omit(account, 'url'), accountInfo);
    }

    async _logout() {
        // Do nothing !
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /// PRIVATE FUNCTIONS
    ///
    /**
     * @return {{ date: Date, type: string, description: string, deposit: number, withdrawal: number, balance: number, account: string, currency: 'CAD' }[]}
     * @private
     */
    _buildStatementUpToNow() {
        let now = new Date();
        let allTransactions = DemoDataCrawler._getStatement(now.getMonth());
        return allTransactions.filter((t) => t.date.getTime() < now.getTime());
    }

    static _getStatement(monthIndex) {
        let now = new Date();
        let yearAndMonth = `${now.getFullYear()}-${(monthIndex + 1).toString().padStart(2, '0')}`;

        let monthly = _.flatten(
            [
                // revenu mensuel
                this._create(`Crédit d'impôt pour solidarité`, `GOUV. QUÉBEC`, 71.58, 1, yearAndMonth, [5]),
                this._createForEveryDayOfWeek(`Paie`, `BOREAL-INFORMATIONS STRATEGIQUE`, 500, 1, yearAndMonth, 4),
                // bouffe
                this._createForEveryDayOfWeek(`Achat`, `MICROBRASSERIE LA MEMP`, -25, 1.15, yearAndMonth, 4),
                this._createForEveryDayOfWeek(`Achat`, `SUBWAY MONDAY !`, -13, 1.1, yearAndMonth, 1),
                this._create(`Achat`, `MCDONALD'S #1337`, -11, 1.1, yearAndMonth, 2),
                this._create(`Achat`, `A&W 5536`, -13, 1.1, yearAndMonth, 1),
                this._createForEveryDayOfWeek(`Achat`, `TIM HORTONS #3740`, -2.2, 1, yearAndMonth, 5),
                this._createForEveryDayOfWeek(`Achat`, `PROVIGO SHERBROOKE #84`, -92, 1.3, yearAndMonth, 6),
                // pret auto
                this._createForEveryDayOfWeek(`Prêt`, `BANQUE DE MONTREAL`, -50, 1, yearAndMonth, 2),
                // factures mensuel
                this._create(`Achat`, `DOMAINE LE MONTAGNAIS`, -333, 1, yearAndMonth, [1]),
                this._create(`Paiement`, `Koodo Mobile`, -35, 1, yearAndMonth, [15]),
                this._create(`Paiement`, `FIZZ`, -44.36, 1, yearAndMonth, [7]),
                this._create(`Assurance`, `ASSURANCE GÉNÉRAL DESJARDINS`, -76.27, 1, yearAndMonth, [21]),
                this._create(null, `Blizzard Entertainment 194-99551380 CA`, -20.88, 1, yearAndMonth, [18]),
                this._create('Achat', `NETFLIX.COM 866-716-0414 ON`, -10.99, 1, yearAndMonth, [28]),
                // transport
                this._create(null, `COUCHETARD #563 SHERBROOKE QC`, -50, 1.15, yearAndMonth, 3),
                // autres
                monthIndex % 3 === 0
                    ? this._create('Achat', `Amazon.ca*FHASHKASDJFH AMAZON.CA ON`, -40, 1.2, yearAndMonth, 1)
                    : [],
                (monthIndex + 1) % 6 === 0
                    ? this._create('Achat', `Pay-to-build-your-own-keyboard CO.`, -750, 1.1, yearAndMonth, 1)
                    : [],
            ],
            true
        );

        // prettier-ignore
        switch(monthIndex) {
            case 0: return _.flatten([
                monthly,
                // Changement d'huile !
                this._create('Achat', 'UNIVERSITY GALT SERVICE #1', -75, 1.2, yearAndMonth, 1),
                this._create(`TPS`, `CANADA`, 120.53, 1, yearAndMonth, [5]),
            ], true);
            case 1: return _.flatten([
                monthly,
            ], true);
            case 2: return _.flatten([
                monthly,
                this._create(`Achat`, `VERTIGE ESCALADE INC`, -495, 1, yearAndMonth, [23]),
            ], true);
            case 3: return _.flatten([
                monthly,
                this._create(`TPS`, `CANADA`, 120.53, 1, yearAndMonth, [5]),
            ], true);
            case 4: return _.flatten([
                monthly,
                // Les pneux d'été ! (au 3 ans)
                (now.getFullYear() % 3 === 0 ? this._create('Achat', 'CANADIAN TIRE #42', -500, 1.1, yearAndMonth, 1) : []),
                // Retour d'impot !
                this._create(`Remboursement d'impôt`, `CANADA`, 500, 1.1, yearAndMonth, 1),
            ], true);
            case 5: return _.flatten([
                monthly,
            ], true);
            case 6: return _.flatten([
                monthly,
                // Réparation d'auto random !
                ((now.getFullYear() - 5) % 8 === 0 ? this._create('Achat', 'UNIVERSITY GALT SERVICE #2', -378, 1.5, yearAndMonth, 1) : []),
                this._create(`TPS`, `CANADA`, 120.53, 1, yearAndMonth, [5]),
            ], true);
            case 7: return _.flatten([
                monthly,
            ], true);
            case 8: return _.flatten([
                monthly,
            ], true);
            case 9: return _.flatten([
                monthly,
                this._create(`TPS`, `CANADA`, 120.53, 1, yearAndMonth, [5]),
            ], true);
            case 10: return _.flatten([
                monthly,
                // Les pneux d'hiver ! (au 6 ans)
                ((now.getFullYear() + 1) % 6 === 0 ? this._create('Achat', 'CANADIAN TIRE #42', -500, 1.1, yearAndMonth, 1) : []),
                this._create(`Achat`, `CORP.SKI & GOLF MONT-ORFORD`, -143.74, 1, yearAndMonth, [25]),
            ], true);
            case 11: return _.flatten([
                monthly,
                // Réparation d'auto random !
                ((now.getFullYear() + 2) % 3 === 0 ? this._create('Achat', 'UNIVERSITY GALT SERVICE #3', -251, 1.3, yearAndMonth, 1) : []),
            ], true);
        }
    }

    static _createForEveryDayOfWeek(type, description, amount, variance, yearAndMonth, dayIndex) {
        let dateSplit = yearAndMonth.split('-');
        let daysInMonth = new Date(dateSplit[0], dateSplit[1], 0).getDate();
        let date = new Date(dateSplit[0], dateSplit[1] + 1, 1);
        let days = [];
        for (let i = 1; i <= daysInMonth; i++) {
            date.setDate(i);
            if (dayIndex === date.getDay()) {
                days.push(i);
            }
        }
        return this._create(type, description, amount, variance, yearAndMonth, days);
    }

    static _create(type, description, amount, variance, yearAndMonth, days) {
        // Add ascii code values of all characters together to form a "random" number
        let random =
            description.split('').reduce((acc, char) => (acc += char.charCodeAt(0)), 0) * new Date().getFullYear();
        let varianceRange = Math.round(Math.abs(amount) * (variance - 1) * 100) / 100; // round to exact penny values
        let getAmount = function () {
            let modifier = varianceRange !== 0 ? random % (varianceRange * 2) : 0;
            // modify the "random" value to get a new one each time !
            random += (1 / modifier) * new Date().getFullYear();
            return Math.abs(amount) + modifier - varianceRange;
        };

        if (typeof days === 'number') {
            // 'days' is the number of pseudo random days to generate.
            let dateSplit = yearAndMonth.split('-');
            let daysInMonth = new Date(dateSplit[0], dateSplit[1], 0).getDate();

            let dayCount = days;
            days = [];
            for (let i = 0; i < dayCount; i++) {
                let day = random % daysInMonth;
                random += (1 / day) * new Date().getFullYear();
                days.push(day + 1);
            }
        } else {
            // 'days' is the exact list of days to create transactions for.
            // Do nothing.
        }

        return days.map((day) => ({
            date: dayjs(`${yearAndMonth}-${day.toString().padStart(2, '0')}`, 'YYYY-MM-DD').toDate(),
            type,
            description,
            deposit: amount > 0 ? getAmount() : null,
            withdrawal: amount <= 0 ? getAmount() : null,
            account: 'demo_data',
            currency: 'CAD',
        }));
    }
}
module.exports = DemoDataCrawler;
