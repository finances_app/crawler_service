module.exports = Object.freeze({
    login: 'login',
    twoFa: '2fa',
    secretQuestion: 'secret_question',
    home: 'home',
    other: 'other'
});