/**
 * This type of error allows the crawler cron job to keep executing.
 * <br/>
 * This error will probably fix itself.
 * (ex: system error on Desjardins's side, site under maintenance, etc..)
 */
class RecoverableError extends Error {
    constructor(message, internalError, isLogoutRequired = true) {
        super(message);
        this.logoutRequired = isLogoutRequired;
        this.internalError = internalError;
    }
}
module.exports = RecoverableError;
