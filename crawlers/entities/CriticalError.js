/**
 * Represent any type of error that could POTENTIALLY lead to Desjardins locking the account.
 * It is important to stop the crawler job for this user if an error of this type happens.
 * <br/>
 * ex: Failed to access the summary page after typing the password (password could be wrong)
 */
class CriticalError extends Error {
    constructor(message, internalError, isLogoutRequired = true) {
        super(message);
        this.logoutRequired = isLogoutRequired;
        this.internalError = internalError;
    }
}
module.exports = CriticalError;
