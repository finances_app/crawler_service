/**
 * @class Account
 */
class Account {
    constructor({ url, name, type, uniqueName, balance }) {
        this.url = url;
        this.name = name;
        this.type = type;
        this.uniqueName = uniqueName;
        this.balance = balance;
    }
}
module.exports = Account;
