class accountType {
    static EOP = 'EOP';
    static CS = 'CS';
    static ES = 'ES';
    static ET = 'ET';
    static MC = 'MC';
    static PR = 'PR';
    static CC = 'CC';

    static isSavingsAccount(type) {
        return [this.EOP, this.CS, this.ES, this.ET].includes(type);
    }

    static isLoanAccount(type) {
        return [this.MC, this.PR].includes(type);
    }

    static isCreditCard(type) {
        return [this.CC].includes(type);
    }

    static getType(accountName) {
        const re = /(?=[\d-]*)(EOP|CS|ES|ET|MC|PR|CC)(?=\d*)/gi;
        let maybeType = re.exec(accountName)[0];
        if (this.isSavingsAccount(maybeType) || this.isLoanAccount(maybeType) || this.isCreditCard(maybeType)) {
            return maybeType;
        }
        return null;
    }
}

module.exports = accountType;
